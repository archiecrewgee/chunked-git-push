from __future__ import annotations  # allows self referential type hinting
from pathlib import Path            # OS agnostic path processing


# each node contains a list of children and a name; todo: proper annotations and folder structure
class Node():
    # initialisation function; for the root node provide no `parent` and the `path` will be recursively searched to produce a tree
    def __init__(self, path: Path, parent: Node = None):
        self.path = path
        self.parent = parent
        self.children = []
        self._size = None

        # assume if no parent is provided then Node is the tree root
        if self.parent == None:
            self.explore_path()
            print(f"No parent specified for node: \n{self}\n\nAssuming '{self.path}' is root and exploring children...")

    # returns the relative path of a node to a given base path, returns `None` if relative path doesn't exist
    def relative_path(self, basePath=None):
        # if no base path is specified then use parent path
        if basePath == None:
            basePath = self.parent.path

        # return relative path, ValueError means the paths are not relative
        try:
            return self.path.relative_to(basePath)
        except ValueError:
            return None

    # gathers all child nodes and returns as a list of nodes
    def children_recursive(self):
        value = [self]
        for child in self.children:
            value += child.children_recursive()
        
        return value


    # all sub pathroutes are explored in a depth-first search
    def explore_path(self):
        # get all nested paths as list (as opposed to generator straight from pathlib)
        nested = [p for p in self.path.glob('*')]

        # return if at file (no nested paths)
        if len(nested) == 0:
            return
        
        # for all nested paths
        for nestedPath in nested:
            # may try to find current path in children (this should not be needed unless tree is explored multiple times)
            # this following line can be used: `not nestedPath.resolve() in [x.path.resolve() for x in self.children]``
            # currently we only assume this is run once and no loops exist so no need to actually perform this check, instead just append the new child 
            self.children.append(Node(nestedPath, self.path))

            # traverse to child and explore
            self.children[-1].explore_path()
    
    # returns the current size of the given node, including all sub-files/-directories, it also evaluates these 
    def size(self):
        # return if already been defined
        if self._size != None:
            return self._size
        
        # if is file and not dir then return file size
        if self.path.is_file():
            self._size = self.path.stat().st_size
        # otherwise - is directory - so size is the sum of all children sizes 
        else:
            self._size = sum([x.size() for x in self.children])

        return self._size
    

    def nice_display(self, depth: int = 0) -> str:
        spacer = "\t"

        # select full or partial path based on depth (is root or not)
        if depth == 0:  dummyPath = str(self.path)
        else:           dummyPath = self.path.parts[-1]

        # provide file/dir type description for display
        if self.path.is_dir():  dummyType = "<dir>"
        else:                   dummyType = ""

        # create spaced name and recurse children
        display = "\n{}{} {} [{}]".format(spacer * depth, dummyPath, dummyType, self.size())
        for child in self.children:
            display += child.nice_display(depth = depth + 1)

        # remove '\n' from start of string if at depth 0
        if depth == 0: display = display[1:]

        return display


    def __str__(self):
        return f"{self.path}\n\tsize: {self.size()}B\n\tchildren: {[c.path for c in self.children]}"

    # comparitor compares the path of two nodes as this is uniquiely identifying
    def __eq__(self, other):
        if type(other) != Node:
            return TypeError
        
        return self.path == other.path