# the partitioner class seperates memory into dustinct chunks 
# memory is treated as a variety of nodes that each contribute to make a chunk

from source.node import Node

from shutil import copytree, copy2

# Chunk class
# contains a list of nodes and the functions to interact with them
class Chunk():
    def __init__(self, nodes = []):
        self.nodes = nodes

    def size(self):
        return sum([n.size() for n in self.nodes])

    # copies a given chunk's nodes to an address
    # the copying is performed realtive to/from a destination/source path
    def copy(self, srcPathBase, dstPathBase):
        print(f"copying from {srcPathBase} -> {dstPathBase}")

        # for all nodes (these may be directories)
        for node in self.nodes:
            pathRelative = node.relative_path(srcPathBase)  # get relative path

            if pathRelative == None:
                print(f"'{node.path}' is not relative to '{srcPathBase}'")
                exit() # todo: replace this with proper raised custom error

            # get destination absolute path
            dstPathAbsolute = dstPathBase / pathRelative
            print(f"absolute path {dstPathAbsolute}")

            # copy tree from source to destination absolute path
            if node.path.is_dir():
                copytree(node.path, dstPathAbsolute, dirs_exist_ok=True)
            else:
                copy2(node.path, dstPathAbsolute)

    def __str__(self):
        returnString = f"Chunk of {len(self.nodes)} nodes and size {self.size()}B"
        for i, node in enumerate(self.nodes):
            returnString += f"\n\t{i}: {node.path}"

            if ~node.path.is_file(): 
                returnString += " <DIR>"

        return returnString


class MemoryPartitioner():
    def __init__(self, tree: Node, maxChunkSize: int):
        self.tree = tree
        self.maxSize = maxChunkSize
        self.chunks = []
        self.largeChunks = []

        self._build_chunks()

    def number_of_chunks(self):
        return len(self.chunks) + len(self.largeChunks)

    
    # given a treee, builds a number of chunks within a file size
    def _build_chunks(self):
        basePath = self.tree.path

        nodesBySize = sorted(self.tree.children_recursive(), key=lambda node: node.size(), reverse=True)        
        self.chunks = []
        self.largeChunks = []

        # take out all files above the maximum chunk size
        self.largeChunks = []

        for node in nodesBySize.copy():
            if node.size() <= self.maxSize:
                break # break for below maximum sizes as node list is ordered according to this attribute 
            
            if node.path.is_file():
                print(f"\nFound large chunk of size {node.size()}B:\n{node}")
                self.largeChunks.append(node)

            # large directories should also be removed from the sorted list although they are not considered distinct large chunks
            nodesBySize.remove(node)

        # create chunks of all sub-maxsize files/directories
        # while un-chunked nodes still exist
        while len(nodesBySize): 
            # pick largest node, get next largest within limit
            dummyChunk = Chunk([])
            for node in nodesBySize.copy():
                # check if maximum packet threshold size would be crossed
                if node.size() + dummyChunk.size() < self.maxSize:
                    # add this node to the active chunk and remove the children from the `nodesBySize` list
                    dummyChunk.nodes.append(node)

                    for child in node.children_recursive():
                        try:
                            nodesBySize.remove(child)
                        except ValueError:                  # ignore if child is no longer in the list (means it has already been removed)
                            continue
                else:
                    break

            print(dummyChunk)
            self.chunks.append(dummyChunk)

        print("large chunks:", len(self.largeChunks))
        print("small chunks:", len(self.chunks))
        

