# Chunked Git Push

The following program was created to make large, single-change, pushes to Git-based hosts. Hosts, such as GitLab, have limited sizes that can be pushed in one packet (on free subscriptions); hence, files must be partitioned to not exceed this.

The solution proposed here takes a given local path and pushes all subdirectories to a given Git host. If the local path contains file's individually larger than the maximum push size then these will be split into binary files that may then be stitched back together via running the `stitch.bat`/`stitch.sh` file after pulling the repository. 

**Note:**

 * The host system is assumed to have Git installed and credentials setup
 * The Git repository interaction is not tested with SSH


## Example

To run the program install the requirements with pip then run `python push.py -h` to see help options