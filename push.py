# TODO:
# readme
# remove looping ability
# proper annotations
# proper folder strucutre
# cli input
# add try-except
#

from __future__ import annotations  # allows self referential type hinting

from pathlib import Path            # OS agnostic path processing
from loguru import logger as log    # plesent logging library
from shutil import rmtree
from os import remove
from math import ceil
from git import Repo
import argparse 

from source.node import Node
from source.partitioner import Chunk, MemoryPartitioner


if __name__ == "__main__":
    # setup CLI  
    parser = argparse.ArgumentParser(description="Pushes directory to a given Git-based repository in chunks to ensure maximum packet size is not reached. This program assumes Git is setup correctly.")
    parser.add_argument('source', help="relative or absolute path of the directory to push to Git")
    parser.add_argument('link', help="link to the Git repository to push to")
    parser.add_argument('-p', "--packet-size", help="maximum size of a chunk in bytes", type=int, default=(80 * (1024**2)))
    parser.add_argument('-n', "--green-pastures", help="remove current contents of Git repository and push in source anew", action='store_true')
    parser.add_argument('-v', "--verbose", help="specifies whether to run program in verbose mode", action='store_true')
    parser.add_argument('-k', "--keep-scratch-dir", help="keep the scratch directory. Note that this will be a sub directory of the source", action='store_true')
    parser.add_argument('-f', "--force", help="will force overwrite of file conflicts in Git repository", action='store_true')

    # parse arguments
    args = parser.parse_args()
    basePath = Path(args.source).resolve()

    # print arguments
    print(f"Arguments:\n\tsource: {basePath}\n\tgit link: {args.link}\n\tpacket size: {args.packet_size}\n\tgreen pastures: {args.green_pastures}\n\tverbose: {args.verbose}\n\tkeep scratch directory: {args.keep_scratch_dir}\n\tforce: {args.force}")

    # initialise directory tree
    tree = Node(basePath)

    print(tree.nice_display())

    # seperate tree into chunks
    partitioner = MemoryPartitioner(tree, args.packet_size)

    try:
        # create temp directory
        tempDir = Path(__file__).parent / ".temp_git_repo"
        tempDir.mkdir(parents=True, exist_ok=True)

        # create local git repository via 'clone command'
        repo = Repo.clone_from(args.link, tempDir)

        # remove contents if specified
        if args.green_pastures:
            for file in tempDir.iterdir():
                # ignore if .git file
                if file.name == ".git": 
                    continue

                if file.is_dir():
                    rmtree(file, ignore_errors=True)
                else:
                    remove(file)

        # push all chunks to git repository
        count = 0

        # push all chunks below the maximum size to the remote git repository
        for chunk in partitioner.chunks:
            count += 1
            print(f"\n--------------------- Pushing chunk {count} ---------------------")
            chunk.copy(tree.path, tempDir)
            repo.git.add(all=True)
            repo.git.commit('-m', f"automated push | {count / partitioner.number_of_chunks() * 100.0}% [{count}/{partitioner.number_of_chunks()}]\n\n{chunk}")
            repo.remotes.origin.push()

        # create basic bat/sh stitching files, ignore if already exists
        if not Path(tempDir / "stitch.bat").exists():
            with open(tempDir / "stitch.bat", "w") as file:
                file.write("@rem Run 'stitch.bat' to build complete files")
        if not Path(tempDir / "stitch.sh").exists():
            with open(tempDir / "stitch.sh", "w") as file:
                file.write("# Run './stitch.sh' to build complete files")

        # push all large chunks to the big repository
        for node in partitioner.largeChunks:
            count += 1

            # prepare variables
            size = node.size()                          # total size of the node
            binarySource = open(node.path, 'rb').read() # binary source file

            print(f"\n--------------------- Pushing large chunk {count} @ '{node.path}' ---------------------\n\ttotal size: {size}\n\tnumber of partial files: {ceil(size / args.packet_size)}")

            sizeCopied = 0  # size is in bytes so this value can also be used to slice the large binary file in question
            i = 0
            partialFilePaths = []
            while sizeCopied < size:
                # open file and write portion of its contents to a new directory
                partialFilePaths.append(f"{node.relative_path(basePath)}_{i}")
                with open(tempDir / partialFilePaths[-1], "wb+") as file:
                    # ensure no slice overflows the total large file size
                    if sizeCopied + args.packet_size > size:
                        print(f"end ->{i * args.packet_size} to end")
                        file.write(binarySource[i * args.packet_size : ])
                    else:
                        print(f"middle -> {i * args.packet_size} to {(i+1) * args.packet_size}")
                        file.write(binarySource[i * args.packet_size : (i+1) * args.packet_size])

                sizeCopied += args.packet_size
                i += 1

                # push copy to remote git repository
                repo.git.add(all=True)
                repo.git.commit('-m', f"automated push | {count / partitioner.number_of_chunks() * 100.0}% [{count}/{partitioner.number_of_chunks()}]\n\n{chunk} | large file: {i} / {ceil(size / args.packet_size)}")
                repo.remotes.origin.push()

            # append new split file to stitching script
            with open(tempDir / "stitch.bat", "a") as file:
                file.write(f"\ncopy /b {" + ".join([f"\"%~dp0\\{x}\"" for x in partialFilePaths])} \"%~dp0\\{node.relative_path(basePath)}\"")
            with open(tempDir / "stitch.sh", "a") as file:
                file.write(f"\ncat {" ".join([f"\"$0/{x}\"" for x in partialFilePaths])} > \"$0/{node.relative_path(basePath)}\"")
                
    except ChildProcessError:
        print("Process call failed") # todo: make this a proper warning, also make this revert all git process      
    except Exception as e:
        raise e
        
    # delete temporary repository
    if not args.keep_scratch_dir:
        rmtree(tempDir) 

exit()
